import React from 'react'; 
import './App.css'; 
import Contacts from './components/contact/Contacts';
import {Provider} from './components/Context'
import Navbar from './components/navbar/Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';

function App() {
  return (
    <Provider> 
        <div className="App"> 
          <Navbar title="REACT DEMO 11" /> 
          <Contacts /> 
        </div> 
  </Provider>

  );

}

export default App;


       {/* <Contact name="ADIL HAMMAD" email="test@test.com" tel="0600220022"/> 
      <Contact name="HAMMAD DEV" email="demo@test.com" tel="0766550022"/>  */}