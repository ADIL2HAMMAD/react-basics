import React, { Component } from 'react'
import Contact from './Contact'
import { Consumer } from '../Context'

class Contacts extends Component {

    DeleteCantact = (id) => {
        const { contacts } = this.state;

        const newContactList = contacts.filter((contact) => contact.id !== id);

        this.setState({
            contacts: newContactList
        })
    }

    render() { 
        return (
            <Consumer>  
                {
                    value =>
                        (
                            <div>
                                {value.contacts.map((contactItem) => (
                                    <Contact data={contactItem} key={contactItem.id} DeleteCantactFromChild={this.DeleteCantact.bind(this, contactItem.id)} />
                                ))}
                            </div>
                        )
                }
            </Consumer>

        )
    }
}


export default Contacts;