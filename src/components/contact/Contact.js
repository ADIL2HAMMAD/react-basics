import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { Consumer } from '../Context';
class Contact extends Component {

    state = {
        showcontact: true
    }


    ShowHide(name) {
        this.setState({
            showcontact: !this.state.showcontact
        })
    }

    DeleteContact = (id ,dispatch) => {  
        dispatch ({
            type : "DELETE_CONTACT",
            payload : id
        })
    }


    render() {
        const { id ,name, email, tel } = this.props.data;
        return (
            <Consumer>
                {
                    value => {
                        const {dispatch} = value ;
                        return (
                            <div className="card">
                            <div className="card-body">
                                <h4 className="card-title">
                                    {name}
                                    <i style={{ cursor: 'pointer' }} onClick={this.ShowHide.bind(this, name)} className="fa fa-sort-down"></i>
                                    <i style={{ color: 'red', cursor: 'pointer', float: 'right' }} 
                                    onClick={this.DeleteContact.bind(this , id , dispatch )} className="fa fa-times"></i>
                                </h4>
                                {(this.state.showcontact) ? (
                                    <div className="card-text">
                                        <ul className="list-group">
                                            <li className="list-group-item ">{tel}</li>
                                            <li className="list-group-item">{email}</li>
                                        </ul>
                                    </div>
                                ) : null}
                            </div>
                        </div> 
                        ) 
                    }
                }
 
            </Consumer>
        )
    }
}

Contact.defaultProps = {
    name: "user",
    email: "my@email.com",
    tel: "0600330033",
}

export default Contact;

