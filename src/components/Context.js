import React, { Component } from 'react'

const Context = React.createContext();

const reducer = (state, action) => {
    switch (action.type) {
        case 'DELETE_CONTACT':
            return {
                contacts: state.contacts.filter((contact) => contact.id !== action.payload)
            }
        default:
            return state;
    }
}

export class Provider extends Component {

    state = {
        contacts: [
            { id: 1, name: "user1", email: "email1@mail.com", tel: "0700000001" },
            { id: 2, name: "user2", email: "email2@mail.com", tel: "0700000002" },
            { id: 3, name: "user3", email: "email3@mail.com", tel: "0700000003" },
            { id: 4, name: "user4", email: "email4@mail.com", tel: "0700000004" }
        ],
        dispatch : action => this.setState (state => reducer(state , action))
    }



    render() {
        return (
            <Context.Provider value={this.state}>
                {this.props.children}
            </Context.Provider>
        )
    }
}

export const Consumer = Context.Consumer;